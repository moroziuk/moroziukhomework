package com.company.HomeWork4;

import java.util.Arrays;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if (year > 2023 || year < 1900) {
            throw new IllegalArgumentException(
                    "Year must be in range 0-9999 but found " + iq);}
    }

    public Human(String name, String surname, int year, int iq,
                 Family family, Day day, String hobby) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = (Family) family;
        this.schedule = new String [][] {{}};

        if (iq > 100 || iq < 0) {
            throw new IllegalArgumentException(
                    "iq must be in range 0-100 but found " + iq);}
        if (year > 2023 || year < 1900) {
            throw new IllegalArgumentException(
                    "Year must be in range 1900-2023 but found " + year);}
    }


    public Human(String name, String surname, int year, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = (Family) family;

        if (year > 2023 || year < 1900) {
            throw new IllegalArgumentException(
                    "Year must be in range 1900-2023 but found " + year);}
    }
    public void greetPet() {
        String petNickname = family.getPet().getNickname();
        System.out.printf("Привіт, %s! %n", petNickname);
    }
    public void describePet() {
        String petSpecies = family.getPet().getSpecies();
        int petAge = family.getPet().getAge();
        int petTrickLevel= family.getPet().getTrickLevel();
        if (petTrickLevel > 50) {

            System.out.printf(
                    "У мене є %s, йому %d років, він дуже хитрий", petSpecies, petAge);
        }
        else System.out.printf(
                "У мене є %s, йому %d років, він майже не хитрий", petSpecies, petAge);
    }

    public  String getName() {
        return name;
    }

    public  void setName(String name) {
        this.name = name;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public String getFamily() {
        return family.toString();
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public  String getSurname() {
        return surname;
    }

    public  void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return String.format("Human{name=:%s, surname:%s, year=:%s,iq=:%s, schedule =%s}",
                getName(),
                getSurname(),
                getYear(),
                getIq(),
                Arrays.toString(schedule)
        );
    }
    // Human{name='Name', surname='Surname',
    //        year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(family, human.family)  && Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    public static void main(String[] args) {
        Human sashko = new Human("Sashko", "Moroziuk", 1994);
        Human yulia = new Human("Yulia", "Kazhan", 1996);
        Pet doggo = new Pet ("Dog","Woofer");
        Pet lem = new Pet ("Cat","Lem");
        Family moroziuk = new Family (yulia, sashko);
        Human oleksa = new Human("Oleksa", "Antons", 1999);
        Human ostap = new Human("Ostap", "Antons", 1996);
        Family antons = new Family (oleksa, ostap);

        moroziuk.setPet(doggo);
        yulia.setFamily(moroziuk);
        sashko.setFamily(moroziuk);

        oleksa.setFamily(antons);
        ostap.setFamily(antons);
        antons.setPet(lem);
        moroziuk.addChild(ostap);

        //System.out.println(ostap.getFamily());
        System.out.println(moroziuk.toString());
    }

}

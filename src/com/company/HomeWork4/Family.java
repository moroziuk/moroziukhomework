package com.company.HomeWork4;

import java.util.Arrays;
import java.util.Objects;

public class Family {

        private Pet pet;
        private Human mother;
        private Human father;
        private Human[] children;

    public Family(Human mother, Human father) {
        this.mother = (Human) mother;
        this.father = (Human) father;
        pet = new Pet();
        children = new Human[0];
    }
   /* єдиною умовою створення нової сім'ї є наявність 2-х батьків,
    причому у батьків має встановлюватися посилання на поточну нову сім'ю,
    а сім'я створюється з порожнім масивом дітей.
    */

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public static Human[] addX(Human arr[], Human x) {
        int i;
        int n = arr.length;
        Human newArr[] = new Human[n + 1];

        for (i = 0; i < n; i++) {
            newArr[i] = arr[i];
            newArr[n] = x;
        }
        return newArr;
    }
    public void addChild(Human child) {

        children = addX(children, child);
        setChildren(children);
    }

    @Override
    public String toString() {
        return String.format(
                getPet().toString() +
                        getMother().toString() +
                        getFather().toString() +
                        Arrays.toString(children)
        );
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(pet, family.pet) && Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(pet, mother, father);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
}

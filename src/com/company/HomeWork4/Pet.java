package com.company.HomeWork4;

import java.util.Arrays;

public class Pet {
    private  String species;
    private  String nickname;
    private  int age;
    private  int trickLevel;
    private  String[] habits;

    public Pet() {
    }

    public Pet(String species, String nickname) {
    this.species = species;
    this.nickname = nickname;
    }

    public Pet(String species, String nickname,
               int age, int trickLevel, String[] habits) {

        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;

        if (trickLevel > 100 || trickLevel < 0) {
            throw new IllegalArgumentException(
                    "trickLevel must be in range 0-100 but found " + trickLevel);}
        }

    public void eat() {
        System.out.println("Я ї'м!");
    }

    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - %d. Я скучив! %n", nickname);
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    public  String getSpecies() {
        return species;
    }

    public  void setSpecies(String species) {
        this.species = species;
    }

    public  String getNickname() {
        return nickname;
    }

    public  void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public  void setAge(int age) {
        this.age = age;
    }

    public  int getTrickLevel() {
        return trickLevel;
    }

    public  void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public  String[] getHabits() {
        return habits;
    }

    public  void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return String.format("%s:{nickname=:%s, age:%s, trickLevel=:%s,habits=:%s}%n",
                species,
                nickname,
                age,
                trickLevel,
                Arrays.toString(habits)
        );
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


}







